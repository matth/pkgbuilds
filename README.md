# Scrumplex PKGBUILDs
This is a repository of all PKGBUILDs I (co-)maintain. [List of all packages on the AUR](https://aur.archlinux.org/packages/?O=0&SeB=m&K=Scrumplex&outdated=&SB=l&SO=d&PP=50&do_Search=Go).

Licensed under [GPL3](LICENSE).

```
    Sefa Eyeoglu's PKGBUILDs
    Copyright (C) 2019 Sefa Eyeoglu <contact@scrumplex.net> (https://scrumplex.net)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
```
